<?php

namespace App\Http\Controllers;

use App\accountTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AccountTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $AccountTypes = accountTypes::all();
        return $AccountTypes;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'subscription_name' => 'required|max:255',
            'subscription_amount' => 'required|numeric',
            'subscription_duration' => 'required|string|max:255',
            'paystack_plan_code' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $AccountTypes = new accountTypes;
        $AccountTypes->subscription_name = $request->subscription_name;
        $AccountTypes->subscription_amount = $request->subscription_amount;
        $AccountTypes->subscription_duration = $request->subscription_duration;
        $AccountTypes->paystack_plan_code = $request->paystack_plan_code;

        $AccountTypes->save();

        $result = array("status"=>200,"message"=>"Account Types Added", "data"=>"success" );
         return json_encode($result);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $AccountTypes = accountTypes::find($id);
        return $AccountTypes;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'subscription_name' => 'required|max:255',
            'subscription_amount' => 'required|numeric',
            'subscription_duration' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $AccountTypes = accountTypes::find($id);
        $AccountTypes->subscription_name = $request->subscription_name;
        $AccountTypes->subscription_amount = $request->subscription_amount;
        $AccountTypes->subscription_duration = $request->subscription_duration;

        $AccountTypes->save();

        $result = array("status"=>200,"message"=>"Account Types Updated", "data"=>"success" );
         return json_encode($result);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $AccountTypes = accountTypes::find($id);
        $AccountTypes->delete();
        $result = array("status"=>200,"message"=>"Account Type Deleted", "data"=>"success" );
        return json_encode($result);
    }
}
