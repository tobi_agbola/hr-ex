<?php

namespace App\Http\Controllers;

use App\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Template = Template::paginate(15);
        return $Template;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Search the resources for values
    */

    public function search(Request $request){

        $search = $request->search_query;
        $search_query = Template::where("template_name",'like', "%$search%")->orWhere("template_category",'like', "%$search%")->orWhere("template_description",'like', "%$search%")->get();
        return $search_query;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'template_name' => 'required|max:255',
            'template_category' => 'required|string|max:255',
            'template_file' => 'required'
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $template = new Template;
        $path = $request->file('template_file')->store('template_file', 'public');
        $template->template_name = $request->template_name;
        $template->template_category = $request->template_category;
        $template->template_description = $request->template_description;
        $template->template_view_count = 0;
        $template->template_url = $path;

        $template->save();

        $result = array("status"=>200,"message"=>"Template Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $Template = Template::find($id);
        return $Template;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'template_name' => 'required|max:255',
            'template_category' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $template = Template::find($id);
        $template->template_name = $request->template_name;
        $template->template_category = $request->template_category;
        $template->template_description = $request->template_description;

        $template->save();

        $result =array("status"=>200,"message"=>"Template Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Template = Template::find($id);
        Storage::delete($Template->template_url);
        $Template->delete();
        $result = array("status"=>200,"message"=>"Template Deleted", "data"=>"success" );
        return json_encode($result);

    }
}
