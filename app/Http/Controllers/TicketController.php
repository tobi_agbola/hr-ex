<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ticketSender;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Ticket = Ticket::paginate(15);
        return $Ticket;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Get UserTickets
     *
     */
    public function getUserTickets(Request $request){
        $user_tickets = Ticket::where("user_id",$request->user_id)->get();
        return $user_tickets;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'subject' => 'required|string|max:255',
            'message' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $Ticket = new Ticket;
        $Ticket->user_id = $request->user_id;
        $Ticket->subject = $request->subject;
        $Ticket->message = $request->message;
        $Ticket->ticket_status = "Request";


        $Ticket->save();

        $this->sendTicket($Ticket->id);

        $result = array("status"=>200,"message"=>"Ticket Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Ticket = Ticket::find($id);
        return $Ticket;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Ticket = Ticket::find($id);
        $Ticket->ticket_status = "Completed";

        $Ticket->save();

        $result = array("status"=>200,"message"=>"Ticket Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'subject' => 'required|string|max:255',
            'message' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $Ticket = Ticket::find($id);
        $Ticket->user_id = $request->user_id;
        $Ticket->subject = $request->subject;
        $Ticket->message = $request->message;



        $Ticket->save();

        $result = array("status"=>200,"message"=>"Ticket Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Ticket = Ticket::find($id);
        $Ticket->delete();
        $result = array("status"=>200,"message"=>"Ticket Deleted", "data"=>"success" );
        return json_encode($result);
    }

    public function sendTicket($id){
        $ticket = Ticket::findOrFail($id);
        Mail::to("hello@hrexng.com")->send(new ticketSender($ticket));
    }
}
