<?php

namespace App\Http\Controllers;

use App\jobRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\mailSender;

class jobRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobRequest = jobRequest::paginate(15);
        return $jobRequest;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Get User Job Request
     *
     */
    public function getJobRequest(Request $request){
        $user_job_request = jobRequest::where("user_email",$request->user_email)->get();
        return $user_job_request;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_name' => 'required|string|max:255',
            'user_id' => 'numeric',
            'user_email' => 'required|email|max:255',
            'subject' => 'required|string|max:255',
            'message' => 'required|string',
            'job_type' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $jobRequest = new jobRequest;
        $jobRequest->user_name = $request->user_name;
        $jobRequest->user_id = $request->user_id;
        $jobRequest->user_email = $request->user_email;
        $jobRequest->subject = $request->subject;
        $jobRequest->message = $request->message;
        $jobRequest->job_type = $request->job_type;

        $jobRequest->save();

        $this->sendJobRequest($jobRequest->id);
        $result = array("status"=>200,"message"=>"Job Request Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobRequest = jobRequest::find($id);
        return $jobRequest;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobRequest = jobRequest::find($id);
        $jobRequest->job_type = "Completed";

        $jobRequest->save();
        $result = array("status"=>200,"message"=>"Job Request Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_name' => 'required|string|max:255',
            'user_id' => 'numeric',
            'user_email' => 'required|email|max:255',
            'subject' => 'required|string|max:255',
            'message' => 'required|string',
            'job_type' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $jobRequest = jobRequest::find($id);
        $jobRequest->user_name = $request->user_name;
        $jobRequest->user_id = $request->user_id;
        $jobRequest->user_email = $request->user_email;
        $jobRequest->subject = $request->subject;
        $jobRequest->message = $request->message;
        $jobRequest->job_type = $request->job_type;

        $jobRequest->save();

        $result = array("status"=>200,"message"=>"Job Request Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jobRequest = jobRequest::find($id);
        $jobRequest->delete();
        $result = array("status"=>200,"message"=>"Job Request Deleted", "data"=>"success" );
        return json_encode($result);
    }

    public function sendJobRequest($id){
        $jobRequest = jobRequest::findOrFail($id);
        Mail::to("hello@hrexng.com")->send(new mailSender($jobRequest));
    }
}
