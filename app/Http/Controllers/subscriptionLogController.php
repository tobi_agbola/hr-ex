<?php

namespace App\Http\Controllers;

use App\subscriptionLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class subscriptionLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptionLog = subscriptionLog::paginate(15);
        return $subscriptionLog;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subscription_type' => 'required|string|max:255',
            'user_id' => 'numeric',
            'subscription_amount' => 'numeric',
            'subscription_start_date' => 'required|date|max:255',
            'subscription_end_date' => 'required|date|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $subscriptionLog = new subscriptionLog;
        $subscriptionLog->subscription_type = $request->subscription_type;
        $subscriptionLog->user_id = $request->user_id;
        $subscriptionLog->subscription_amount = $request->subscription_amount;
        $subscriptionLog->subscription_start_date = $request->subscription_start_date;

        $subscriptionLog->save();

        $result = array("status"=>200,"message"=>"Subcription Logged Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subscriptionLog = subscriptionLog::find($id);
        return $subscriptionLog;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'subscription_type' => 'required|string|max:255',
            'user_id' => 'numeric',
            'subscription_amount' => 'numeric',
            'subscription_start_date' => 'required|date|max:255',
            'subscription_end_date' => 'required|date|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $subscriptionLog = subscriptionLog::find($id);
        $subscriptionLog->subscription_type = $request->subscription_type;
        $subscriptionLog->user_id = $request->user_id;
        $subscriptionLog->subscription_amount = $request->subscription_amount;
        $subscriptionLog->subscription_start_date = $request->subscription_start_date;

        $subscriptionLog->save();

        $result = array("status"=>200,"message"=>"Subcription Logged Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscriptionLog = subscriptionLog::find($id);
        $subscriptionLog->delete();
        $result = array("status"=>200,"message"=>"Subscription Log", "data"=>"success" );
        return json_encode($result);
    }
}
