<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $User = User::paginate(15);
        return $User;
    }

        /**
     * Get User Email
     *
     */
    public function getUserDetailsViaID(Request $request){
        $user_details = User::where("id",$request->id)->get();
        return $user_details;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function Subscribe(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric|max:255',
            'subscription_type' => 'required|string|max:255',
            'subscription_duration' => 'required|numeric',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $User = User::find($request->user_id);
        $User->current_subscription_status = $request->subscription_type;
        $User->subscription_start_date = date("Y-m-d h:i:s");
        $User->subscription_end_date = date("Y-m-d h:i:s", strtotime('+'.$request->subscription_duration. 'months'));

        $User->save();
        $result = array("status"=>200,"message"=>"Subscription Made", "data"=>"success" );
        return json_encode($result);


    }
    public function getSubscriptionStatus(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'company_name' => 'required|string|max:255',
            'role' => 'required|in:admin,user',
            'phone_number' => 'required|numeric|min:11',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $User = new User;
        $User ->firstname = $request->firstname;
        $User ->lastname = $request->lastname;
        $User ->company_name = $request->company_name;
        $User ->role = $request->role;
        $User ->phone_number = $request->phone_number;
        $User ->email = $request->email;
        $User ->password = Hash::make($request->password);



        $User ->save();

        $result = array("status"=>200,"message"=>"User Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $User = User::find($id);
        return $User;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $User = User::find($id);
        $role;
        if($User->role=="user"){

            $role = "admin";
            $User->role = "admin";

        }else{

            $role = "user";
            $User->role = "user";

        }

        $User ->save();

        $result = array("status"=>200,"message"=>"Role toggled to".$role, "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'company_name' => 'required|string|max:255',
            'role' => 'required|in:admin,user',
            'phone_number' => 'required|numeric|min:11',
            'email' => 'required|string|email|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $User = User::find($id);
        $User ->firstname = $request->firstname;
        $User ->lastname = $request->lastname;
        $User ->company_name = $request->company_name;
        $User ->role = $request->role;
        $User ->phone_number = $request->phone_number;
        $User ->email = $request->email;

        $User ->save();

        $result = array("status"=>200,"message"=>"User Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $User = User::find($id);
        $User->delete();
        $result = array("status"=>200,"message"=>"User Deleted", "data"=>"success" );
        return json_encode($result);
    }
}
