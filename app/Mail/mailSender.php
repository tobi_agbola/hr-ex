<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\jobRequest;

class mailSender extends Mailable
{
    use Queueable, SerializesModels;

    public $jobRequest;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(jobRequest $JobRequest)
    {
        $this->jobRequest = $JobRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.toHrex');
    }
}
