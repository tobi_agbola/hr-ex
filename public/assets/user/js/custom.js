$(document).ready(function() {
        // $(window).on('load', function(){
        //     $(".loader").fadeOut("slow");
        // });
    $(".loader").fadeOut("slow");
    var typed = new Typed('#typed', {
        strings: ["Let our HR experts help take the weight off"],
        typeSpeed: 70,
        backDelay: 3000,
        loop:true,
        backSpeed:70
    });
    // var typed2 = new Typed('#typed2', {
    //     strings: ["Looking for expert HR support at affordable rates.","After a more proactive approach to managing your HR.",
    //                 "Not willing to invest in a fully resourced HR department for now.","Looking to focus on growing your Business and rely on seasoned experts for HR advisory and support."],
    //     typeSpeed: 30,
    //     backDelay: 8000,
    //     loop:true,
    //     backSpeed:10
    // });

    $("#about").click(function(){
        $('html,body').animate({
        scrollTop: $("#aboutDiv").offset().top
        },1000);
    });
    $("#aboutFooter").click(function(){
        $('html,body').animate({
        scrollTop: $("#aboutDiv").offset().top
        },1000);
    });
    $("#whyhrex").click(function(){
        $('html,body').animate({
        scrollTop: $("#whyusDiv").offset().top
        },1000);
    });
    $("#packages").click(function(){
        $('html,body').animate({
        scrollTop: $("#packagesDiv").offset().top
        },1000);
    });
    $("#whyhrexFooter").click(function(){
        $('html,body').animate({
        scrollTop: $("#whyusDiv").offset().top
        },1000);
    });
    $("#services").click(function(){
        $('html,body').animate({
        scrollTop: $("#servicesDiv").offset().top
        },1000);
    });
    $("#servicesFooter").click(function(){
        $('html,body').animate({
        scrollTop: $("#servicesDiv").offset().top
        },1000);
    });
    $("#packagesFooter").click(function(){
        $('html,body').animate({
        scrollTop: $("#packagesDiv").offset().top
        },1000);
    });
});
