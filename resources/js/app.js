
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('admintemplate-component', require('./components/AdminTemplateComponent.vue'));
Vue.component('adminjobrequest-component', require('./components/AdminJobRequestComponent'));
Vue.component('adminticket-component', require('./components/AdminTicketComponent'));
Vue.component('adminuser-component', require('./components/AdminUserComponent'));
Vue.component('adminaccounttype-component', require('./components/AdminAccountTypeComponent'));
Vue.component('adminhome-component', require('./components/AdminHomeComponent'));
Vue.component('userhome-component', require('./components/UserHomeComponent'));
Vue.component('userprofile-component', require('./components/UserProfileComponent?'));
Vue.component('userticket-component', require('./components/UserTicketComponent'));
Vue.component('userjobrequest-component', require('./components/UserJobRequestComponent'));
Vue.component('contactus-component', require('./components/ContactUsComponent.vue?'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
