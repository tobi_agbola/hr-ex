@extends('layouts.admin-app')
    @section('title','Dashboard | Accounts')
    @section('header-title','HR-EX ADMIN PORTAL')
    @section('content')
    <div class="container-fluid px-xl-5" id="app">
            <adminaccounttype-component></adminaccounttype-component>
            @csrf
    </div>
    <script>
        $("#admin-account").addClass("active");
    </script>
    @endsection

