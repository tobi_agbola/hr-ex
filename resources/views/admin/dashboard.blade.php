@extends('layouts.admin-app')
    @section('title','Dashboard')
    @section('header-title','HR-EX ADMIN PORTAL')
    @section('content')
        <div class="container-fluid px-xl-5" id="app">
                <adminhome-component></adminhome-component>
                @csrf
        </div>
    <script>
        $("#dashboard").addClass("active");
    </script>
    @endsection

