@extends('layouts.admin-app')
    @section('title','Dashboard | Job Request')
    @section('header-title','HR-EX ADMIN PORTAL')
    @section('content')
        <div class="container-fluid px-xl-5" id="app">
            <adminjobrequest-component></adminjobrequest-component>
            @csrf
        </div>
    <script>
        $("#admin-job-request").addClass("active");
    </script>
    @endsection

