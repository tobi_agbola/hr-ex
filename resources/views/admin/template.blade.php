@extends('layouts.admin-app')
    @section('title','Dashboard | Template')
    @section('header-title','HR-EX ADMIN PORTAL')
    @section('content')
        <div class="container-fluid px-xl-5" id="app">
            <admintemplate-component></admintemplate-component>
            @csrf
        </div>
        <script>
                $("#admin-template").addClass("active");
        </script>
    @endsection

