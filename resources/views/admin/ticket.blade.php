@extends('layouts.admin-app')
    @section('title','Dashboard | Tickets')
    @section('header-title','HR-EX ADMIN PORTAL')
    @section('content')
        <div class="container-fluid px-xl-5" id="app">
            <adminticket-component></adminticket-component>
            @csrf
        </div>
        <script>
            $("#admin-ticket").addClass("active");
        </script>
    @endsection

