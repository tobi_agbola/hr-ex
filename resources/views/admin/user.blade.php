@extends('layouts.admin-app')
    @section('title','Dashboard | Users')
    @section('header-title','HR-EX ADMIN PORTAL')
    @section('content')
        <div class="container-fluid px-xl-5" id="app">
                <adminuser-component></adminuser-component>
                @csrf
        </div>
        <script>
            $("#admin-user").addClass("active");
        </script>
    @endsection

