@extends('layouts.main-app')
    @section('title','Member Log-in')

    @section('content')
    <section class="slide-section">
        <div class="slide">
            <img src="{{ asset('assets/user/assets/images/slide/contactus.jpg')}}" alt="slide" class="image-responsive slide-image" />
            <div class="slide-content">

            <p class="slide-text">
              <span  id="contactTyped"></span><br>
            </p>
            </div>
        </div>
    </section>
    <section class="about-us-section" id="aboutDiv">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 justify-content-center  align-self-center">
                <h3 class="section-title text-center">Member Log-in</h3>
                    <form method="POST" action="{{ route('login') }}">
                            @csrf
                        <div class="form-group">
                            <label class="label-text" for="email">Email address</label>
                            <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="Email" aria-describedby="emailHelp" placeholder="Email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="label-text" for="password">Password</label>
                            <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                            <button type="submit" class="btn btn-primary reg-btn">Login</button>
                    </form>
                </div>
                <div class="col-lg-6 justify-content-center  align-self-center text-center">
                    <div class="image-box">
                        <img src="{{ asset('assets/user/assets/images/pages/login.jpg') }}" alt="Contact Us" class="whoweare-image grow" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection
    @section('title-script')
        <script>
            var typed3 = new Typed('#contactTyped', {
                strings: ["Login"],
                typeSpeed: 30,
                backDelay: 8000,
                loop:false,
                backSpeed:30
            });
        </script>
    @endsection
