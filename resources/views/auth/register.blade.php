@extends('layouts.main-app')
    @section('title','Member registration’ ')

    @section('content')
    <section class="slide-section">
        <div class="slide">
            <img src="assets/user/assets/images/slide/contactus.jpg" alt="slide" class="image-responsive slide-image" />
            <div class="slide-content">

            <p class="slide-text">
              <span  id="contactTyped"></span><br>
            </p>
            </div>
        </div>
    </section>
    <section class="about-us-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 justify-content-center  align-self-center">
                <h3 class="section-title text-center">{{ __('Register') }}</h3>
                    <form method="POST" action="{{ route('register') }}">
                            @csrf
                        <div class="form-group">
                            <label class="label-text" for="fullname">First name</label>
                            <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="Firstname" name="firstname" value="{{ old('name') }}" placeholder="First name" required autofocus>
                            @if ($errors->has('firstname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="label-text" for="fullname">Last name</label>
                            <input type="text" class="form-control {{ $errors->has('lastname') ? ' is-invalid' : '' }}" id="Lastname" name="lastname" placeholder="Last name" required>
                            @if ($errors->has('lastname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('lastname') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="label-text" for="Email">Email address</label>
                            <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="Email" name="email" aria-describedby="emailHelp" placeholder="Enter email" required>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="label-text" for="phoneno">Phone Number</label>
                            <input type="number" class="form-control {{ $errors->has('phone_number') ? ' is-invalid' : '' }}" id="phoneno" name="phone_number" placeholder="Phone Number" required>
                            @if ($errors->has('phone_number'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                </span>
                        @endif
                        </div>
                        <div class="form-group">
                            <label class="label-text" for="companyName">Company Name</label>
                            <input type="text" class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" id="companyName" name="company_name" placeholder="Company Name" required>
                            @if ($errors->has('company_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('company_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="label-text" for="subject">Password</label>
                            <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" placeholder="Password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="label-text" for="retypepassword">Retype Password</label>
                            <input type="password" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" id="retypepassword" name="password_confirmation" placeholder="Retype Password" required>
                            <input type="hidden" name="role" value="user" required>
                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                            <button type="submit" class="btn btn-primary reg-btn">Sign Up</button>
                    </form>
                </div>
                <div class="col-lg-6 justify-content-center text-center">
                    <div class="image-box">
                        <img src="assets/user/assets/images/pages/signup.jpg" alt="Contact Us" class="whoweare-image grow" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection
    @section('title-script')
    <script>
        var typed3 = new Typed('#contactTyped', {
            strings: ["Sign Up"],
            typeSpeed: 30,
            backDelay: 8000,
            loop:false,
            backSpeed:30
        });
    </script>
    @endsection
