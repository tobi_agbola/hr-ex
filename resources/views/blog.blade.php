@extends('layouts.main-app')
    @section('title','Blog')

    @section('content')
    <section class="slide-section">
        <div class="slide">
            <img src="{{ asset('assets/user/assets/images/slide/contactus.jpg') }}" alt="slide" class="image-responsive slide-image" />
            <div class="slide-content">

            <p class="slide-text">
              <span  id="blogTyped"></span><br>
            </p>
            </div>
        </div>
    </section>
    <section class="about-us-section">
            <div class="container-fluid">
                <h3 class="section-title text-center">Blog</h3>
                <div class="row">
                    <div class="col-sm-4 align-self-center">
                        <div class="card card-shadow">
                            <div class="card-body">
                                    <h4 class="b-title text-center">Lorem ipsum dolor sit amet</h4>
                                    <img src="{{ asset('assets/user/assets/images/pages/whyhrex.jpg') }}" alt="Who Are We" class="b-image img-thumbnail" />
                                <p class="b-content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                                <button class="btn btn-primary blog-btn">Read More</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 align-self-center">
                        <div class="card card-shadow">
                            <div class="card-body">
                                    <h4 class="b-title text-center">Lorem ipsum dolor sit amet</h4>
                                    <img src="{{ asset('assets/user/assets/images/pages/whyhrex.jpg') }}" alt="Who Are We" class="b-image img-thumbnail" />
                                <p class="b-content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                                <button class="btn btn-primary blog-btn">Read More</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 align-self-center">
                            <div class="card card-shadow">
                                <div class="card-body">
                                        <h4 class="b-title text-center">Lorem ipsum dolor sit amet</h4>
                                        <img src="{{ asset('assets/user/assets/images/pages/whyhrex.jpg') }}" alt="Who Are We" class="b-image img-thumbnail" />
                                    <p class="b-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                    <button class="btn btn-primary blog-btn">Read More</button>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <br>
        </section>
@endsection
@section('title-script')
    <script>
        var typed3 = new Typed('#blogTyped', {
            strings: ["Blog"],
            typeSpeed: 30,
            backDelay: 8000,
            loop:false,
            backSpeed:30
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARri2tNqQ_uHCkwdWNkkSu8tc3zehn16Q&callback=initMap" async></script>
@endsection
