@extends('layouts.main-app')
    @section('title','Contact Us')

    @section('content')
    <section class="slide-section">
        <div class="slide">
            <img src="{{ asset('assets/user/assets/images/slide/contactus.jpg') }}" alt="slide" class="image-responsive slide-image" />
            <div class="slide-content">

            <p class="slide-text">
              <span  id="contactTyped"></span><br>
            </p>
            </div>
        </div>
    </section>
    <section class="about-us-section" id="aboutDiv">
        <div class="container" >
                <div class="row">
                        <div class="col-lg-6 justify-content-center  align-self-center" id="app">
                            <contactus-component></contactus-component>
                        </div>
                        <div class="col-lg-6 justify-content-center  align-self-center text-center">
                            <div class="image-box">
                                <img src="{{ asset('assets/user/assets/images/pages/contactus1.jpg') }}" alt="Contact Us" class="whoweare-image grow" />
                            </div>
                            <h5 class="section-title text-center"><i class="fas fa-phone"></i> Call Us on 080 9040 2809</h5>
                        </div>
                    </div>

        </div>
    </section>
@endsection
@section('title-script')
    <script>
        var typed3 = new Typed('#contactTyped', {
            strings: ["Contact Us"],
            typeSpeed: 30,
            backDelay: 8000,
            loop:false,
            backSpeed:30
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARri2tNqQ_uHCkwdWNkkSu8tc3zehn16Q&callback=initMap" async></script>
@endsection
