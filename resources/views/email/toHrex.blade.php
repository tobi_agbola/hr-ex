@extends('layouts.email-template')
@section('content')
From: {{$jobRequest->user_name}}
Email: {{$jobRequest->user_email}}
Subject: {{$jobRequest->subject}}
Job Type: {{$jobRequest->job_type}}
<br><br>
  {{$jobRequest->message}}
<br><br>

@endsection
