@extends('layouts.admin-app')
@section('title','Dashboard')
@section('header-title','HR-EX PORTAL')
@section('content')
<div class="container-fluid px-xl-5" id="app">
        <userhome-component></userhome-component>
        @csrf
</div>
<script>
    $("#home").addClass("active");
</script>
@endsection

