<div id="sidebar" class="sidebar py-3">
    <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
    @if (Auth::user()->role === "admin")
        <ul class="sidebar-menu list-unstyled">
        <li class="sidebar-list-item"><a href="{{route('admin-dashboard')}}" class="sidebar-link text-muted" id="dashboard"><i class="o-home-1 mr-3 text-gray"></i><span>Home</span></a></li>
                <li class="sidebar-list-item"><a href="{{route('admin-user')}}" class="sidebar-link text-muted" id="admin-user"><i class="o-sales-up-1 mr-3 text-gray"></i><span>Users</span></a></li>
                <li class="sidebar-list-item"><a href="{{route('admin-template')}}" class="sidebar-link text-muted" id="admin-template"><i class="o-table-content-1 mr-3 text-gray"></i><span>Templates</span></a></li>
                <li class="sidebar-list-item"><a href="{{route('admin-ticket')}}" class="sidebar-link text-muted" id="admin-ticket"><i class="o-survey-1 mr-3 text-gray"></i><span>Tickets</span></a></li>
                <li class="sidebar-list-item"><a href="{{route('admin-job-request')}}" class="sidebar-link text-muted" id="admin-job-request"><i class="o-survey-1 mr-3 text-gray"></i><span>Job Requests</span></a></li>
                <li class="sidebar-list-item"><a href="{{route('admin-account')}}" class="sidebar-link text-muted" id="admin-account"><i class="o-survey-1 mr-3 text-gray"></i><span>Account Types</span></a></li>
                <li class="sidebar-list-item"><a  href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Logout</span></a></li>
        </ul>
    @endif
    @if (Auth::user()->role === "user")
        <ul class="sidebar-menu list-unstyled">
                <li class="sidebar-list-item"><a href="{{route('home')}}" class="sidebar-link text-muted" id="home"><i class="o-home-1 mr-3 text-gray"></i><span>Home</span></a></li>
                <li class="sidebar-list-item"><a href="{{route('user-ticket')}}" class="sidebar-link text-muted" id="user-ticket"><i class="o-sales-up-1 mr-3 text-gray"></i><span>Tickets</span></a></li>
                <li class="sidebar-list-item"><a href="{{route('user-profile')}}" class="sidebar-link text-muted" id="user-profile"><i class="o-table-content-1 mr-3 text-gray"></i><span>Profile</span></a></li>
                <li class="sidebar-list-item"><a href="{{route('user-job-request')}}" class="sidebar-link text-muted" id="user-job-request"><i class="o-survey-1 mr-3 text-gray"></i><span>Job Requests</span></a></li>
                <li class="sidebar-list-item"><a  href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Logout</span></a></li>
        </ul>
    @endif

</div>
