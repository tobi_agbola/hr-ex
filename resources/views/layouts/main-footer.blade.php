<footer>
    <div class="container-fluid footer-top">
        <div class="row">
            <div class="col-sm-4 text-center">
                <img src="{{ asset('assets/user/assets/images/logo/footerLogo.fw.png') }}" alt="footer-logo"  />
            </div>
            @if (\Route::current()->getName() == 'welcome')
                <div class="col-sm-2">
                    <ul class="footer-ul">
                        <li class="footer-li text-center">
                        <a class="footer-a" href="{{route('welcome')}}">Home</a>
                        </li>
                        <li class="footer-li text-center">
                            <a class="footer-a welcome-view-only" href="javascript:void(0)" id="aboutFooter">About Us</a>
                        </li>
                        <li class="footer-li text-center">
                            <a class="footer-a welcome-view-only" href="javascript:void(0)" id="servicesFooter">Services</a>
                        </li>
                        <li class="footer-li text-center">
                            <a class="footer-a welcome-view-only" href="javascript:void(0)" id="whyhrexFooter">Why HR-EX</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2">

                    <ul class="footer-ul">
                    <li class="footer-li text-center">
                            <a class="footer-a welcome-view-only" href="javascript:void(0)" id="packagesFooter">Packages</a>
                        </li>
                        {{-- <li class="footer-li text-center">
                            <a class="footer-a" href="{{ route('user-login') }}">Member Log-in</a>
                        </li>
                        <li class="footer-li text-center">
                            <a class="footer-a" href="{{ route('signup') }}">Member registration</a>
                        </li> --}}
                        <li class="footer-li text-center">
                            <a class="footer-a" href="{{ route('contactus') }}">Contact Us</a>
                        </li>
                        <li class="footer-li text-center">
                            <a class="footer-a" href="{{ route('blog') }}">Blog</a>
                        </li>
                    </ul>
                </div>
            @else
                <div class="col-sm-4">
                    <ul class="footer-ul">
                        <li class="footer-li text-center">
                        <a class="footer-a" href="{{route('welcome')}}">Home</a>
                        </li>
                        {{-- <li class="footer-li text-center">
                            <a class="footer-a" href="{{ route('user-login') }}">Member Log-in</a>
                        </li>
                        <li class="footer-li text-center">
                            <a class="footer-a" href="{{ route('signup') }}">Member registration</a>
                        </li> --}}
                        <li class="footer-li text-center">
                            <a class="footer-a" href="{{ route('contactus') }}">Contact Us</a>
                        </li>
                        <li class="footer-li text-center">
                            <a class="footer-a" href="{{ route('blog') }}">Blog</a>
                        </li>
                    </ul>
                </div>
            @endif
            <div class="col-sm-4 text-center">
                <h3 class="footer-header">Social Media</h3>
                <i class="fab fa-facebook-square footer-icon"></i>
                <i class="fab fa-instagram footer-icon"></i>
                <i class="fab fa-twitter-square footer-icon"></i>
                <i class="fab fa-whatsapp-square footer-icon"></i>
            </div>
        </div>
    </div>
    <div class="container-fluid footer-bottom">
        <div class="row">
            <div class="col-sm-12">
                <p class="copyright text-center">&copy; Copyright <?php echo date("Y"); ?>. HR-EX - All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>
