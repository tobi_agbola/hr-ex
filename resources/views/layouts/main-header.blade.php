<div class="loader"></div>
<div class="container-fluid fixed-header">
    <div class="row">
        <div class="col-lg-12">
            <header>
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark white-header">
                    <!-- Brand -->
                    <a class="navbar-brand no-margin" href="{{route('welcome')}}"><img src="{{ asset('assets/user/assets/images/logo/headerLogo.fw.png') }}" alt="Logo"/></a>

                    <!-- Toggler/collapsibe Button -->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <!-- Navbar links -->
                    <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
                            @if (\Route::current()->getName() == 'welcome')
                                <ul class="navbar-nav ui-nav">
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link" href="javascript:void(0)" id="about">About Us</a>
                                    </li>
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link" href="javascript:void(0)" id="whyhrex">Why HR-EX</a>
                                    </li>
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link" href="javascript:void(0)" id="services">Our Services</a>
                                    </li>
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link" href="javascript:void(0)" id="packages">Our Packages</a>
                                    </li>
                                    {{-- <li class="nav-item header-item">
                                        <a class="nav-link header-link" href="javascript:void(0)" id="testimonial">Testimonial</a>
                                    </li> --}}
                                    {{-- <li class="nav-item header-item">
                                        <a class="nav-link header-link" href="{{ route('user-login') }}" >Member Log-in</a>
                                    </li>
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link" href="{{ route('signup') }}">Member registration</a>
                                    </li> --}}
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link" href="{{ route('contactus') }}">Contact Us</a>
                                    </li>
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link" href="{{ route('blog') }}" id="blog">Blog</a>
                                    </li>
                                </ul>
                            @else
                                <ul class="navbar-nav ui-nav" style="width: 28%">
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link add-left-padding" href="{{ route('welcome') }}">Home</a>
                                    </li>
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link add-left-padding" href="{{ route('blog') }}">Blog</a>
                                    </li>
                                    {{-- <li class="nav-item header-item">
                                        <a class="nav-link header-link add-left-padding" href="{{ route('user-login') }}" >Member Log-in</a>
                                    </li>
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link add-left-padding" href="{{ route('signup') }}">Member registration</a>
                                    </li> --}}
                                    <li class="nav-item header-item">
                                        <a class="nav-link header-link add-left-padding" href="{{ route('contactus') }}">Contact Us</a>
                                    </li>
                                </ul>
                            @endif
                    </div>
                </nav>
            </header>

        </div>
    </div>
</div>
