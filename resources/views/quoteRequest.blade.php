@extends('layouts.main-app')
    @section('title','Quote Request')

    @section('content')
    <section class="slide-section">
        <div class="slide">
            <img src="{{ asset('assets/user/assets/images/slide/contactus.jpg') }}" alt="slide" class="image-responsive slide-image" />
            <div class="slide-content">

            <p class="slide-text">
              <span  id="contactTyped"></span><br>
            </p>
            </div>
        </div>
    </section>
    <section class="about-us-section" id="aboutDiv">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 justify-content-center  align-self-center">
                <h3 class="section-title text-center">Quote Request</h3>
                    <form>
                        <div class="form-group">
                            <label class="label-text" for="fullname">Full name</label>
                            <input type="text" class="form-control" id="fullname" placeholder="Full name">
                        </div>
                        <div class="form-group">
                            <label class="label-text" for="Email">Email address</label>
                            <input type="email" class="form-control" id="Email" aria-describedby="emailHelp" placeholder="Enter email">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label class="label-text" for="subject">Request Title</label>
                            <input type="text" class="form-control" id="subject" placeholder="Request Title">
                        </div>
                        <div class="form-group">
                            <label class="label-text" for="Body">Request Details</label>
                            <textarea class="form-control" id="Body" rows="3"></textarea>
                        </div>
                            <button type="submit" class="btn btn-primary reg-btn">Send</button>
                    </form>
                </div>
                <div class="col-lg-6 justify-content-center  align-self-center text-center">
                    <div class="image-box">
                        <img src="{{ asset('assets/user/assets/images/pages/requestQuote.jpg') }}" alt="Contact Us" class="whoweare-image grow" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection
    @section('title-script')
        <script>
            var typed3 = new Typed('#contactTyped', {
                strings: ["Request Quote"],
                typeSpeed: 30,
                backDelay: 8000,
                loop:false,
                backSpeed:30
            });
        </script>
    @endsection
