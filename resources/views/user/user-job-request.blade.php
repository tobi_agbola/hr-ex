@extends('layouts.admin-app')
@section('title','Dashboard | Job Request')
@section('header-title','HR-EX PORTAL')
@section('content')
    <div class="container-fluid px-xl-5" id="app">
            <userjobrequest-component></userjobrequest-component>
            @csrf
            <input type="hidden" id="user_email" value="{{ Auth::user()->email }}" />
            <input type="hidden" id="user_name" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" />

    </div>
    <script>
            $("#user-job-request").addClass("active");
    </script>
@endsection

