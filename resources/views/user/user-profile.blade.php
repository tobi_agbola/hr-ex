@extends('layouts.admin-app')
@section('title','Dashboard | Profile')
@section('header-title','HR-EX PORTAL')
@section('content')
    <div class="container-fluid px-xl-5" id="app">
            <userprofile-component></userprofile-component>
            @csrf
            <input type="hidden" id="user_id" value="{{ Auth::user()->id }}" />
            <input type="hidden" id="user_email" value="{{ Auth::user()->email }}" />
    </div>
        <script>
            $("#user-profile").addClass("active");
        </script>
@endsection

