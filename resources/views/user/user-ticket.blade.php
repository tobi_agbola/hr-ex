@extends('layouts.admin-app')
@section('title','Dashboard | Ticket')
@section('header-title','HR-EX PORTAL')
@section('content')
    <div class="container-fluid px-xl-5" id="app">
            <userticket-component></userticket-component>
            @csrf
            <input type="hidden" id="userid" value="{{ Auth::user()->id }}" />
    </div>
<script>
    $("#user-ticket").addClass("active");
</script>
@endsection

