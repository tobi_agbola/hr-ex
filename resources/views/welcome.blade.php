
@extends('layouts.main-app')
    @section('title','Home ')
{{-- <script>
    $(".welcome-view-only").show();
    alert("test");
</script> --}}
    @section('content')
    <section class="slide-section">
            <div class="slide">
                <img src="{{ asset('assets/user/assets/images/slide/slide2.jpg') }}" alt="slide" class="image-responsive slide-image" />
                <div class="round-slide-overlay">
                    <img src="{{ asset('assets/user/assets/images/logo/SliderLogo.fw.png') }}" alt="Logo" class="logo-slider" />
                </div>
                <div class="slide-content">

                <p class="slide-text">
                    We Want To Help You Grow,<br> <span  id="typed"></span><br>
                    <a href="{{ route('contactus') }}"><button class="btn btn-primary reg-btn">Learn More</button></a>
                </p>
                </div>
            </div>
        </section>
        <section class="about-us-section page-size" id="aboutDiv">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 align-self-center">
                            <h3 class="section-title text-center">About HR-EX</h3>
                            <p class="p-content">
                                    HR-EX Consulting partners with micro, small and medium enterprises looking for affordable and accessible HR professional services and advisory. Our vision is to be a technology company that provides professional HR services to micro, small and medium enterprises.
                                    At HR-EX Consulting, we believe small businesses are the growth engine of any economy and having the right people and organizational processes, practices and systems in place is fundamental to securing the future and scaling the Business.
                                    Because we understand that your business needs evolve as you grow,
                                    and you should only really be paying for services you need when you need them, providing professional HR services at affordable rates is at the heart of what we do.

                            </p>
                    </div>
                    <div class="col-lg-6 justify-content-center text-center align-self-center">
                        <div class="image-box">
                            <img src="{{ asset('assets/user/assets/images/pages/whoweare.jpg') }}" alt="Who Are We" class="whoweare-image grow" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="video-section page-size" id="video">
                <div class="container ">
                    <div class="row">
                        <div class="col-lg-6 align-self-center offset-lg-3">
                            <h3 class="section-title text-center">Promotional Video</h3>
                            <div class="video-box">
                                <video class="video-tag" controls>
                                <source src="{{ asset('assets/user/assets/video/promovideo.mp4') }}" type="video/mp4">
                                <!-- <source src="mov_bbb.ogg" type="video/ogg"> -->
                                Your browser does not support HTML5 video.
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        {{-- <section class="why-us-section" id="whyusDiv">
                        <div class="div-relative">
                            <div class="overlaydiv"></div>
                            <div class="content-why-us">
                                <h2 class="section-title text-center">Why HR-ex</h2>
                                <p class="why-us-content">
                                    HR-EX is for you if you are <span class="section-title why-us-span white" id="typed2"></span>
                                </p>
                            </div>
                        </div>
                        <img src="{{ asset('assets/user/assets/images/pages/WhyUS.jpg') }}" alt="Why HR-ex" class="image-responsive" />
        </section> --}}
        <section class="why-us-section_sec page-size" id="whyusDiv">
        <br>
        <h2 class="section-title text-center">Why HR-EX</h2>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 text-center align-self-center">
                        <div class="image-box">
                            <img src="{{ asset('assets/user/assets/images/pages/whyhrex.jpg') }}" alt="Who Are We" class="whoweare-image grow" />
                        </div>
                    </div>
                    <div class="col-lg-6 text-center align-self-center">
                    <div>
                        <p class="why-us-content">
                            HR-EX is for you if you are
                        </p>
                        <ul class="why_us_sec">
                            <li class="li-offer">Looking for expert HR support at affordable rates</li>
                            <li class="li-offer">After a more proactive approach to managing your HR </li>
                            <li class="li-offer">Not willing to invest in a fully resourced HR department for now</li>
                            <li class="li-offer">Looking to focus on growing your Business and rely on seasoned experts for HR advisory and support</li>
                        </ul>
                        <br>
                    </div>
                    </div>
                </div>
            </div>
        <br>
        </section>

        <section class="services-section" id="servicesDiv">
            <div class="container">
                <br>
                <br>
                <h3 class="section-title text-center">Our Services</h3>
                <div class="row">
                        <div class="col-sm-2 text-center swing">
                        </div>
                    <div class="col-sm-4 text-center swing">
                        <img src="{{ asset('assets/user/assets/icons/operator.svg') }}" alt="Daily" class="service-icon-reponsive text-center" />
                        <h6 class="services-header">Day to day or Operational HR Services</h6>
                        <br>
                    </div>
                    {{-- <div class="col-sm-4 text-center swing">
                        <img src="{{ asset('assets/user/assets/icons/cloud-computing.svg') }}" alt="Rocket" class="service-icon-reponsive text-center" />
                        <h6 class="services-header"></h6>
                        <br>
                    </div> --}}
                    <div class="col-sm-4 text-center swing">
                        <img src="{{ asset('assets/user/assets/icons/projector-screen2.svg') }}" alt="Virtual" class="service-icon-reponsive  text-center" />
                        <br>
                        <h6 class="services-header">Strategic HR Support</h6>
                        <br>
                    </div>
                    <div class="col-sm-2 text-center swing">
                    </div>
                    {{-- <div class="col-sm-4 text-center swing">
                        <img src="{{ asset('assets/user/assets/icons/service.svg') }}" alt="Support" class="service-icon-reponsive text-center" />
                        <h6 class="services-header">On-demand HR Support and Services</h6>
                        <br>
                    </div> --}}
                </div>
            </div>
            <div class="container">
                <br>
                <h5 class="services-header text-center">Our HR Service Delivery Model</h5>
                <div class="row justify-content-md-center">
                        <div class="col-lg-12 text-center align-self-center">
                                <p class="p-content">
                                    Our service delivery model comprises 3 integrated components which through a co-creation and partnering process will help fulfil your people and organizational needs
                                </p>
                        </div>
                    <div class="col-lg-10 col-sm-12 text-center align-self-center">
                        <img src="{{ asset('assets/user/assets/images/serviceD.png') }}" alt="Virtual" class="image-responsive  text-center"/>
                    </div>
                </div>
            </div>
            <br>
            <br>
        </section>
        {{-- <section class="delivery-model-section page-size" id="servicesmodelDiv">

        </section> --}}
        <section class="services-section packages small-page-size" id="packagesDiv">
            <div class="container-fluid">
                <h3 class="section-title text-center white">Our Packages</h3>
                <div class="row">
                    <div class="col-sm-12">
                        <p class="p-content white font-regular text-center">
                            <strong>Our packages have been designed with your needs and affordability in mind while also ensuring amazing value for money. </strong></p>
                    </div>
                    <div class="col-md-4 text-center grow">
                        <div class="card card-shadow package-size">
                            <div class="card-body">
                                <h3 class="packages-title">Starter Club</h3>
                                {{-- <h2 class="packages-cash"><span class="package-currency">₦</span><del>20000</del></h2> --}}
                                <h6 class="font-regular center"><strong>Recommended for Start-ups and Micro Businesses</strong></h6>
                                <ul class="package-ul">
                                    <li class="package-li">Online access to customizable templates and tools to get you started and HR compliant in no time</li>
                                    <li class="package-li">Remote access to HR experts to provide HR advisory on customized documents</li>
                                    <li class="package-li">Remote access to HR advisory on your people and organization issues (8hrs per month)</li>
                                </ul>
                                <br>
                                <a href="{{ route('contactus') }}"><button class="btn btn-primary subscribe-btn">CONTACT US NOW</button></a>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="col-md-4 text-center grow">
                        <div class="card card-shadow package-size">
                            <div class="card-body">
                                <h4 class="packages-title">Business Professional Club</h4>
                                {{-- <h2 class="packages-cash"><span class="package-currency">₦</span><del>40000</del></h2> --}}
                                <h6 class="font-regular center"><strong>Recommended for Small and Medium Businesses</strong></h6>
                                <ul class="package-ul">
                                    {{-- <li class="package-li">Online access to customizable templates and tools to get you started and HR compliant in no time</li> --}}
                                    <li class="package-li">Full scope HR Audit</li>
                                    <li class="package-li">Remote day to day HR advisory- hands on HR strategic and operational support</li>
                                    <li class="package-li">Monthly client site visit</li>
                                    <li class="package-li">8 hours access to our HR helpline to provide advise when you need it</li>
                                </ul>
                                <a href="{{ route('contactus') }}"><button class="btn btn-primary subscribe-btn">CONTACT US NOW</button></a>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="col-md-4 text-center grow">
                        <div class="card card-shadow package-size">
                            <div class="card-body">
                                <h6 class="packages-title">On Demand HR Support</h6>
                                <!-- <h2 class="packages-cash"><span class="package-currency">₦</span>50000</h2> -->
                                <h6 class="font-regular center"><strong>Recommended for Special Projects</strong></h6>
                                <ul class="package-ul">
                                    <li class="package-li">Hassle-free access to urgent HR help</li>
                                    {{-- <li class="package-li">Support ad hoc or unforeseen HR needs including backfills, redundancy projects, recruitment drives</li> --}}
                                    <li class="package-li">Support ad hoc or unforeseen HR needs</li>
                                    <li class="package-li">Support for backfills, redundancy projects, recruitment drives</li>
                                </ul>
                                <a href="{{ route('contactus') }}"><button class="btn btn-primary subscribe-btn">CONTACT US NOW</button></a>
                            </div>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </section>
        {{-- <section class="why-us-section_sec page-size">
            <div class="container-fluid">
                <h3 class="section-title text-center">Testimonials</h3>
                <div class="row">
                    <div class="col-sm-4 align-self-center">
                        <div class="card card-shadow">
                            <div class="card-body">
                                    <p class="t-header">Agbola Tobi</p>
                                    <p class="t-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 align-self-center">
                        <div class="card card-shadow">
                            <div class="card-body">
                                <p class="t-header">Agbola Tobi</p>
                                <p class="t-content ">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-4 align-self-center">
                        <div class="card card-shadow">
                            <div class="card-body">
                                <p class="t-header">Agbola Tobi</p>
                                <p class="t-content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </section> --}}
        {{-- <section class="about-us-section page-size">
            <div class="container-fluid">
                <h3 class="section-title text-center">What We Offer</h3>
                <div class="row">
                    <div class="col-md-6 text-center align-self-center">
                        <div class="image-box">
                            <img src="{{ asset('assets/user/assets/images/pages/whatweoffer.jpg') }}" alt="Who Are We" class="whoweare-image grow" />
                        </div>
                    </div>
                    <div class="col-md-6 align-self-center">
                        <ul class="ul-offer">
                            <li class="li-offer">HR Audit and compliance assessments</li>
                            <li class="li-offer">Setting up and building an effective HR function</li>
                            <li class="li-offer">Job description preparation and review</li>
                            <li class="li-offer">People attraction and selection</li>
                            <li class="li-offer">Terminations and workforce reductions</li>
                            <li class="li-offer">Organization design and restructuring</li>
                            <li class="li-offer">Employee disciplinary action</li>
                            <li class="li-offer">Employee performance improvement</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section> --}}
    @endsection
