<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('templates', 'TemplateController');
Route::resource('tickets', 'TicketController');
Route::resource('jobrequest', 'jobRequestController');
Route::resource('accounttypes', 'AccountTypesController');
Route::resource('subscriptionlog', 'SubscriptionLogController');
Route::resource('user', 'userController');
Route::get('/templates/{search_query}/search', 'TemplateController@search');
Route::get('/tickets/{user_id}/show', 'TicketController@getUserTickets');
Route::get('/jobrequest/{user_email}/show', 'jobRequestController@getJobRequest');
Route::get('/user/{user_id}/show', 'userController@getUserDetailsViaID');
Route::post('/user/subscribe', 'userController@Subscribe');
