<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('guest')->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('isuser');

Route::get('/user-login', function () { return view('auth.login'); })->name('user-login')->middleware('guest');

Route::get('/blog', function () { return view('blog'); })->name('blog');

Route::get('/signup', function () { return view('auth.register'); })->name('signup')->middleware('guest');

Route::get('/contactus', function () { return view('contactus'); })->name('contactus')->middleware('guest');

Route::get('/quoterequest', function () { return view('quoteRequest'); })->name('quoterequest')->middleware('guest');

Route::get('/admin', function () { return view('admin.login'); })->name('admin')->middleware('guest');

Route::get('/user-profile', function () { return view('user.user-profile'); })->name('user-profile')->middleware('auth','isuser');

Route::get('/user-ticket', function () { return view('user.user-ticket'); })->name('user-ticket')->middleware('auth','isuser');

Route::get('/user-job-request', function () { return view('user.user-job-request'); })->name('user-job-request')->middleware('auth','isuser');

Route::get('/admin-dashboard', function () { return view('admin.dashboard'); })->name('admin-dashboard')->middleware('auth','isadmin');

Route::get('/admin-user', function () { return view('admin.user'); })->name('admin-user')->middleware('auth','isadmin');

Route::get('/admin-ticket', function () { return view('admin.ticket'); })->name('admin-ticket')->middleware('auth','isadmin');

Route::get('/admin-profile', function () { return view('admin.profile'); })->name('admin-profile')->middleware('auth','isadmin');

Route::get('/admin-template', function () { return view('admin.template'); })->name('admin-template')->middleware('auth','isadmin');

Route::get('/admin-job-request', function () { return view('admin.jobrequest'); })->name('admin-job-request')->middleware('auth','isadmin');

Route::get('/admin-account', function () { return view('admin.account'); })->name('admin-account')->middleware('auth','isadmin');

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
